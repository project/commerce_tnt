<?php

/**
 * @file
 * Defines the TNT shipping method and services for Drupal Commerce.
 */


/**
 * Implements hook_menu().
 */
function commerce_tnt_menu() {
  $items = array();

  $items['admin/commerce/config/shipping/methods/tnt/edit'] = array(
    'title' => 'Edit',
    'description' => 'Adjust TNT shipping settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_tnt_settings_form'),
    'access arguments' => array('administer shipping'),
    'file' => 'commerce_tnt.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'weight' => 0,
  );

  return $items;
}


/**
 * Implements hook_commerce_shipping_method_info().
 */
function commerce_tnt_commerce_shipping_method_info() {
  $shipping_methods = array();

  $shipping_methods['tnt'] = array(
    'title' => t('TNT'),
    'description' => t('Quote rates from TNT'),
  );

  return $shipping_methods;
}


/**
 * Implements hook_commerce_shipping_service_info().
 */
function commerce_tnt_commerce_shipping_service_info() {
  $shipping_services = array();

  $available_services = _commerce_tnt_service_list();
  $selected_services = variable_get('commerce_tnt_services', array());

  foreach ($selected_services as $id => $val) {
    if ($val != 0) {
      $service = $available_services[$id];
      $shipping_services[$service['slug']] = array(
        'title' => $service['title'],
        'description' => $service['description'],
        'display_title' => $service['title'],
        'shipping_method' => 'tnt',
        'price_component' => 'shipping',
        'callbacks' => array(
          'rate' => 'commerce_tnt_service_rate_order',
        ),
      );
    }
  }
  return $shipping_services;
}


/**
 * Returns an array of shipping method rates obtained from the TNT servers.
 *
 * @param array $shipping_service
 *   The shipping service that is being requested by commerce shipping.
 * @param object $order
 *   The commerce order object for the order that we're requesting rates for.
 *
 * @return array
 *   The rate values for the requested shipping service.
 */
function commerce_tnt_service_rate_order($shipping_service, $order) {
  // First attempt to recover cached shipping
  // rates for the current order.
  $rates = commerce_shipping_rates_cache_get('commerce_tnt', $order, variable_get('commerce_tnt_rates_timeout', 0));

  // If no rates were recovered from the cache or the
  // cached rates are over one minute old...
  if (!$rates) {
    $rates = array();

    // Variables for watchdog
    $vars = array();
    $vars['%order_id'] = $order->order_id;

    include_once 'commerce_tnt.xml.inc';

    // Build the rate request for the current order. This returns XML.
    $rate_request_xml = commerce_tnt_build_rate_request($order);

    // If we got a valid rate request object back...
    if ($rate_request_xml) {

      // Submit the API request to TNT.
      $response = commerce_tnt_api_request($rate_request_xml);

      // Remove the password from request so that not logged to watchdog
      $rate_request_xml['password'] = str_repeat('*', strlen($rate_request_xml['password']));
      // Add the request to the vars for debugging
      $vars['%request'] = print_r($rate_request_xml, TRUE);

      if(!empty($response)) {

        if (isset($response->ratedTransitTimeResponse->ratedProducts->ratedProduct)) {
          // Parse the response to cache all requested rates.
          foreach ($response->ratedTransitTimeResponse->ratedProducts->ratedProduct as $rate) {
            // Extract the service name and price information.
            $service_name = commerce_tnt_commerce_shipping_service_name((string) $rate->product->code);
            $decimal = (string) $rate->quote->price;
            $currency_code = (string) $rate->quote->attributes()->currency;
            // Add an item to the rates array for the current service.
            $rates[$service_name] = array(
              'amount' => commerce_currency_decimal_to_amount($decimal, $currency_code),
              'currency_code' => $currency_code,
              'data' => array(),
            );
          }
          // Cache the calculated rates for subsequent requests.
          commerce_shipping_rates_cache_set('commerce_tnt', $order, $rates);
        }

        elseif (isset($response->ratedTransitTimeResponse->brokenRules->brokenRule)) {
          $vars['%code'] = (string)$response->ratedTransitTimeResponse->brokenRules->brokenRule->code;
          $vars['%desc'] = (string)$response->ratedTransitTimeResponse->brokenRules->brokenRule->description;
          watchdog('commerce_tnt',
                   'Response broken rule error for %order_id: %code %desc: %request', $vars, WATCHDOG_ERROR);
        }

        elseif (isset($response->error)) {
          $vars['%code'] = (string)$response->error->code;
          $vars['%desc'] = (string)$response->error->description;
          watchdog('commerce_tnt',
                   'Response error for order %order_id: %code %desc: %request', $vars, WATCHDOG_ERROR);
        }
      }
      else {
        watchdog('commerce_tnt',
                 'Empty response for order %order_id: %request', $vars, WATCHDOG_ERROR);
      }
    }
    else {
      watchdog('commerce_tnt',
               'Empty rate request XML for order %order_id', $vars, WATCHDOG_ERROR);
    }
  }
  return isset($rates[$shipping_service['name']]) ? $rates[$shipping_service['name']] : FALSE;
}


/**
 * A function to return a shipping service name given a shipping service code.
 */
function commerce_tnt_commerce_shipping_service_name($service_code) {
  $service_names = _commerce_tnt_service_list();
  if(!empty($service_names[$service_code]['slug'])) {
    return $service_names[$service_code]['slug'];
  }
}


/**
 * Implements hook_commerce_price_component_type_info().
 */
function commerce_tnt_commerce_price_component_type_info() {
  return array(
    'tnt_road_express' => array(
      'title' => t('TNT Road Express'),
      'weight' => 20,
    ),
  );
}


/**
 * Helper function to get tnt codes for their services.
 */
function _commerce_tnt_service_list() {
  $services = array(
    '76' => array(
      'title' => t('Road Express'),
      'description' => t('Road Express'),
    ),
    '75' => array(
      'title' => t('Overnight Express'),
      'description' => t('Overnight Express'),
    ),
    '712' => array(
      'title' => t('Overnight Delivery by 9.00am'),
      'description' => t('Overnight Delivery by 9.00am'),
    ),
    '717B' => array(
      'title' => t('Technology Express - Sensitive Express'),
      'description' => t('Technology Express - Sensitive Express'),
    ),
    '717' => array(
      'title' => t('Technology Express - Premium Express'),
      'description' => t('Technology Express - Premium Express'),
    ),
    '701' => array(
      'title' => t('Sameday Domestic'),
      'description' => t('Sameday Domestic'),
    ),
  );

  /* Make a unique ID to identify the service by */
  foreach ($services as $key => $service) {
    $service['slug'] = str_replace(' ', '_', drupal_strtolower($service['title']));
    $services[$key] = $service;
  }
  return $services;
}

